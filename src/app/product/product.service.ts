import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductContainer } from './product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  urlInclParams = 'https://27gmrimn45.execute-api.eu-west-2.amazonaws.com/demos/leighton-demo-api?TableName=products';

  headers = {
    headers: {
      'x-api-key': 'zQo4PPqD862IwDIQRZub8gX4dqjA3aW2DDhI6UF4',
    },
  };

  constructor(private http: HttpClient) { }

  getProducts(): Observable<ProductContainer> {
    return this.http.get<ProductContainer>(this.urlInclParams, this.headers); // GET data from API, return an Observable of type Product
  }
}
