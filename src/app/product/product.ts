export interface Product {
    brand: string;
    colour: string;
    description: string;
    name: string;
    productid: string;
    size: {
        small: string,
        medium: string,
        large: string
    };
    type: string;
}

export interface ProductContainer { // This exactly matches the format of data that is returned from the API
    Items: Product[];
    Count: number;
    ScannedCount: number;
}
