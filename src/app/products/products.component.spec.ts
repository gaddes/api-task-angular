import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsComponent } from './products.component';
import { ProductComponent } from '../product/product.component';

import { HttpClientModule } from '@angular/common/http';
import { componentRefresh } from '@angular/core/src/render3/instructions';
import { HttpClientTestingModule } from '@angular/common/http/testing'; // This allows us to test mock API calls

describe('ProductsComponent', () => {
  let component: ProductsComponent;
  let fixture: ComponentFixture<ProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:  [
        HttpClientModule,
        HttpClientTestingModule
      ],
      declarations: [
        ProductsComponent,
        ProductComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('is a simple string test', () => {
    expect('hello').toBe('hello');
  });
});
