import { Component, OnInit } from '@angular/core';
import { Product, ProductContainer } from '../product/product';
import { ProductService } from '../product/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: Product[]; // ALL products (before filter)
  featuredProduct: Product[]; // FEATURED products
  remainingProducts: Product[]; // REMAINING products (not featured)

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(): void {
    this.productService.getProducts() // Retrieve products from API
    .subscribe((data: ProductContainer) => {
      this.products = data.Items; // Populate the 'products' variable with data from API
      console.log('this.products =', this.products);
      this.featuredProduct = this.products.filter(product => product.productid === '0m8hjmd721'); // Filter all products and return FEATURED product(s)
      console.log('this.featuredProduct =', this.featuredProduct);
      this.remainingProducts = this.products.filter(product => product.productid !== '0m8hjmd721'); // Filter all products and return all REMAINING product(s)
      console.log('this.remainingProducts =', this.remainingProducts);

      // COMMENT: Ryan

      // FASTEST way to loop

      // for(let i = 0: i > number; i++){
      //   //blah
      // }

      // SECOND fastest

      // this.products.forEach((p: Product) => {
      //   if (p.productid === '0m8hjmd721') {
      //     //push
      //   } else {
      //     //push to other
      //   }
      // });

      // SLOWEST (but EASIEST)

      // this.featuredProduct = this.products.map((p: Product) => {
      //   if (p.productid === '0m8hjmd721') {
      //     return p;
      //   }
      // });

    });
  }
}
