import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  /**
   * COMMENT - Ryan -> So if you dont have any logic in your component, you normally dont need to import OnInit
   * export class HeaderComponent {
   * }
   * This called a lifecycle hook, read up on it - https://angular.io/guide/lifecycle-hooks
   *
   **/


}
