import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavComponent } from './nav/nav.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { NavLinksComponent } from './nav/nav-links/nav-links.component';
import { NavBannerComponent } from './nav/nav-banner/nav-banner.component';

import { HttpClientModule } from '@angular/common/http'; // <-- Enable HTTP services


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:  [
        HttpClientModule
      ],
      declarations: [
        AppComponent,
        ProductsComponent,
        ProductComponent,
        HeaderComponent,
        NavComponent,
        NavLinksComponent,
        NavBannerComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
